package com.sschudakov.easyminesweeper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class StartActivity extends Activity implements View.OnClickListener {

    // границы допустимых значений числа строк
    private static final int COLUMNS_MIN = 2;
    private static final int COLUMNS_MAX = 10;
    // границы допустимых значений числа строк
    private static final int ROWS_MIN = 2;
    private static final int ROWS_MAX = 10;


    // код результата проверки
    private static final int ALERT_NONE = 0; // параметры введены верно
    private static final int ALERT_COLUMNS = 1; // некорректное число столбцов
    private static final int ALERT_ROWS = 2; // некорректное число строк
    private static final int ALERT_MINES = 3; // некорректное число мин

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Button mButton;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);
        mButton = (Button) findViewById(R.id.btnBegin);
        mButton.setOnClickListener(this);

        EditText rowsEditor = (EditText)findViewById(R.id.editRows);
        EditText colsEditor = (EditText)findViewById(R.id.editColumns);
        EditText minesEditor = (EditText)findViewById(R.id.editMines);

        // Задаем значения по умолчанию
        colsEditor.setText("7");
        rowsEditor.setText("9");
        minesEditor.setText("10");
    }

    @Override
    public void onClick(View v) {
        EditText rowsEditor = (EditText)findViewById(R.id.editRows);
        EditText colsEditor = (EditText)findViewById(R.id.editColumns);
        EditText minesEditor = (EditText)findViewById(R.id.editMines);

        int cols = Integer.parseInt(colsEditor.getText().toString());
        int rows = Integer.parseInt(rowsEditor.getText().toString());
        int mines = Integer.parseInt(minesEditor.getText().toString());

        int alertCode = checkInputParameters(cols, rows, mines);
        if (alertCode != ALERT_NONE) {
            showDialog(alertCode);
            return;
        }

//        Передача введенных данных в FieldActivity
        Intent intent = new Intent();

        intent.setClass(this, MinesweeperGame.class);

        intent.putExtra(MinesweeperGame.EXT_COLS, cols);

        intent.putExtra(MinesweeperGame.EXT_ROWS, rows);

        intent.putExtra(MinesweeperGame.EXT_MINES, mines);

//        запускаем игру, завершаемMainActivity

        startActivity(intent);

//        finish();
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        DialogInterface.OnClickListener doNothing = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {}
        };

        int alertMessage;

        switch (id)
        {
            case ALERT_COLUMNS:
                alertMessage = R.string.alert_columns;
                break;
            case ALERT_ROWS:
                alertMessage = R.string.alert_rows;
                break;
            case ALERT_MINES:
                alertMessage = R.string.alert_mines;
                break;
            default:
                return null;
        }

        return new AlertDialog.Builder(this)
                .setMessage(alertMessage)
                .setNeutralButton(R.string.ok, doNothing)
                .create();
    }

    private int checkInputParameters(int cols, int rows, int mines) {
        if (cols < COLUMNS_MIN || cols > COLUMNS_MAX) return ALERT_COLUMNS;
        if (rows < ROWS_MIN || rows > ROWS_MAX) return ALERT_ROWS;
        if (mines > (rows * cols) - 1) return ALERT_MINES;
        return ALERT_NONE;
    }

}
